import '../styles/index.scss';

window.$ = window.jQuery = require('jquery');

require('slick-carousel');
require('jquery-mask-plugin');
require('parsleyjs');

jQuery(document).ready(function ($) {

    //MASKINPUTS
    (function () {
        var TelefoneMaskBehavior = function (val) {
                return val.replace(/\D/g, '').length === 11 ? '(00) 00000-0000' : '(00) 0000-00009';
            },
            TelefoneMaskOptions = {
                onKeyPress: function (val, e, field, options) {
                    field.mask(TelefoneMaskBehavior.apply({}, arguments), options);
                }
            };

        $('.telefone').mask(TelefoneMaskBehavior, TelefoneMaskOptions);
        $('.cpf').mask('000.000.000-00', {reverse: true});
        $('.cep').mask('00000-000');
    })();

    $(function () {
        var $sections = $('.form-section');
        var $slider = $('.slider');
        var $progressBar = $('.progress');
        var $numberBar = $('.number span');
        var $progressBarLabel = $('.slider__label');

        function navigateTo(index) {
            // Mark the current section with the class 'current'
            $sections
                .removeClass('current')
                .eq(index)
                .addClass('current');
            // Show only the navigation buttons that make sense for the current section:
            $('.form-navigation .previous').toggle(index > 0);
            var atTheEnd = index >= $sections.length - 1;
            $('.form-navigation .next').toggle(!atTheEnd);
            $('.form-navigation [type=submit]').toggle(atTheEnd);

            var calc = ((index) / ($sections.length - 1)) * 100;
            var calNum = ((index) + ($sections.length - 1));

            $progressBar
                .css('background-size', calc + '50% 100%')
                .attr('aria-valuenow', calc);

            $numberBar.text(calNum);

        }

        function curIndex() {
            // Return the current index by looking at which section has the class 'current'
            return $sections.index($sections.filter('.current'));
        }

        // Previous button is easy, just go back
        $('.form-navigation .previous').click(function () {
            navigateTo(curIndex() - 1);
        });

        // Next button goes forward iff current block validates
        $('.form-navigation .next').click(function () {
            $('.demo-form').parsley().whenValidate({
                group: 'block-' + curIndex()
            }).done(function () {
                navigateTo(curIndex() + 1);
            });
        });

        // Prepare sections by setting the `data-parsley-group` attribute to 'block-0', 'block-1', etc.
        $sections.each(function (index, section) {
            $(section).find(':input').attr('data-parsley-group', 'block-' + index);
        });
        navigateTo(0); // Start at the beginning
    });

    $('.slider-depoimentos').slick({
        false: true,
        arrows: false,
        infinite: false,
        speed: 600,
        slidesToShow: 3,
        slidesToScroll: 1,
        responsive: [
            {
                breakpoint: 1024,
                settings: {
                    slidesToShow: 3,
                    slidesToScroll: 1
                }
            },
            {
                breakpoint: 600,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 2
                }
            },
            {
                breakpoint: 480,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1,
                    centerMode: true,
                    centerPadding: '0px'
                }
            }
        ]
    });

});
