
### instalação

```
npm install
```

### Start Dev Server

```
npm start
```

### Build Prod Version

```
npm run build
```

### Tecnologias:

* ES6 via [babel](https://babeljs.io/) (v7)
* SASS [sass-loader](https://github.com/jtangelder/sass-loader)
* Linting [eslint-loader](https://github.com/MoOx/eslint-loader)

